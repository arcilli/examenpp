def main():
    #definire stari
    stari=["0 cents deposited",  #starea 0
           "5 cents deposited",  #starea 1
           "10 cents deposited", #starea 2
           "15 cents deposited", #starea 3
           "20 cents or more deposited" #starea 4
           ]
    #definire succesiune de tranzitii, in functie de nickel sau dim
    tranzitii=[
        #din starea 0
        {
            "nickel" :1,
            "dime" :2
            },
        #din starea 1
        {
            "nickel" :2,
            "dime" :3
            },
        #din starea 2
        {
            "nickel" :3,
            "dime" :4
            },
        #din starea 3
        {
            "nickel" :4,
            "dime" :4
            },
        #din starea 4
        {
            "nickel" :1,
            "dime" :2
            },
        ]

    print("Starea curenta este: %s" % stari[0])
    print("Starea curenta este: %s" % stari[1])
    print("Starea curenta este: %s" % stari[2])
    print("Starea curenta este: %s" % stari[3])
    print("Starea curenta este: %s" % stari[4])
    
    stareInitiala=input("Stare initiala")
    while stareInitiala not in range(0,4):
        stareInitiala=input("Reintrodu stare initiala")

    stareCurenta=stareInitiala
    print("Stare curenta este%s" % stari[stareCurenta])
    print("Introduceti valori pentru monedele de introdus in automat: ")

    while True:
        valMoneda=input("2-iesire, 0-nickel, 1-dime")
        if valMoneda==2:
           break
        elif valMoneda==1:
            stareCurenta=tranzitii[stareCurenta]["dime"]
        elif valMoneda==0:
            stareCurenta=tranzitii[stareCurenta]["nickel"]
        print("Stare curenta ca val: %d" %stareCurenta)
        print("Starea curenta este: %s" % stari[stareCurenta])
main()
