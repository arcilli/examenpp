from logic import *

def main():
    #citire valori de intrare de la tastatura
    a=input("A=")
    b=input("B=")
    c=input("C=")

    #creare obiecte - porti logice
    n1=Not('NOT1')
    n2=Not('NOT2')
    n3=Not('NOT3')

    a1=And('AND1')
    a2=And('AND2')
    a3=And('AND3')
    a4_1=And('AND4_1')
    a4_2=And('AND4_2')

    o1=Or('OR1')
    o2=Or('OR2')
    o_final=Or('OR_f')

    #creare conexiuni
    n2.B.connect(a1.A)
    n3.B.connect(a1.B)
    n1.B.connect(a3.A)
    n2.B.connect(a4_1.B)
    n3.B.connect(a4_2.A)
    a4_1.C.connect(a4_2.B)
    a1.C.connect(o1.A)
    a2.C.connect(o1.B)
    a3.C.connect(o2.A)
    a4_2.C.connect(o2.B)
    o1.C.connect(o_final.A)
    o2.C.connect(o_final.B)

    #monitorizare iesiri
    o_final.C.monitor=1

    #setare intrari
    n1.A.set(a)
    n2.A.set(b)
    n3.A.set(c)
    a2.A.set(a)
    a2.B.set(b)
    a3.B.set(b)
    a4_1.A.set(a)

main()
