
public class Automat {
	private boolean iesire;

	private class Stare{
		private int[] nrStare;

		public Stare() {
			nrStare=new int[2];
			nrStare[0]=nrStare[1]=0;
		}

		public Stare(String stare) {
			nrStare=new int[2];
			nrStare[1]=Integer.parseInt(stare.charAt(1)+"");
			nrStare[0]=Integer.parseInt(stare.charAt(0)+"");
			if (stare.equals("11"))
				iesire=true;
		}

		public String getStare() {
			return nrStare[1]+""+nrStare[0];
		}

		public void setStare(String stare) {
			System.out.println("Stare curenta: " + stare);
			nrStare[1]=Integer.parseInt(stare.charAt(1)+"");
			nrStare[0]=Integer.parseInt(stare.charAt(0)+"");
			if (stare.equals("11"))
				iesire=true;
			System.out.println("Stare dupa schimbare: " +nrStare[1]+""+nrStare[0]);
		}
	}

	private Stare stareCurenta;
	public Automat()
	{
		stareCurenta=new Stare();
	}

	public Automat(String stareInitiala) {
		stareCurenta=new Stare(stareInitiala);
	}

	public void tranziteaza(boolean intrare)
	{
		if (stareCurenta.getStare().equals("00")) {
			if (intrare == false)	
				stareCurenta.setStare("10");
			else
				stareCurenta.setStare("01");
		}
		else if(stareCurenta.getStare().equals("01")){
			if (intrare == false)
				stareCurenta.setStare("10");
			else
				stareCurenta.setStare("11");
		}
		else if(stareCurenta.getStare().equals("10")) {
			if (intrare==false) 
				stareCurenta.setStare("11");
			else
				stareCurenta.setStare("01");
		}
		else if (stareCurenta.getStare().equals("11")) {
			if (intrare==false)
				stareCurenta.setStare("10");
			else
				stareCurenta.setStare("01");
		}
		if(iesire==true)
			System.out.println("Iesirea este 1");
	}
	public static void main(String[] args) {
		Automat a=new Automat("00");
		a.tranziteaza(false);
		a.tranziteaza(false);
		a.tranziteaza(true);
		a.tranziteaza(true);
		a.tranziteaza(false);
	}
}
