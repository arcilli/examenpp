
public interface Stiva <E>{
	public boolean push (E val);
	public E peek();
	public E pop();
	public boolean isEmpty();
}
