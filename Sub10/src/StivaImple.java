
public class StivaImple<E>  implements Stiva<E>{
	private E[] array;
	private int numberOfElements=0;
	private int capacity;
	
	//constructor
	public StivaImple(int val){
		capacity=val;
		array=(E[]) (new Object[capacity]);
	}
	//metode
	@Override
	public boolean push (E val) {
		if (numberOfElements<=capacity-1)
		{
			array[numberOfElements++]=val;
			return true;
		}
		System.out.println("Nu s-a putut adauga, capacitate limitata");
		return false;
	}
	
	@Override
	public E peek() {
		if (numberOfElements != 0)
		{
			return array[numberOfElements-1];
		}
		System.out.println("Nu am la ce sa fac peek");
		return null;
	}
	
	@Override
	public E pop()
	{
		if (numberOfElements != 0)
		{
			E val=array[--numberOfElements];
			//array[numberOfElements+1]=null;
			return val;
		}
		System.out.println("Nu am la ce sa fac pop, stiva goala");
		return null;
	}
	
	@Override
	public boolean isEmpty() {
		return (numberOfElements==0);
	}
	
	public int getNrOfElements() {
		return numberOfElements;
	}
}
