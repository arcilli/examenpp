import java.awt.*;
import java.awt.event.*;

public class Notepad extends Frame implements ActionListener{
	private MenuBar baraMeniu;
	private Menu meniu;
	private ScrollPane panou;
	private TextArea text;
	private MenuItem nou, deschide, iesire;
	
	public Notepad() {
		super("Notepad");
		setPreferredSize(new Dimension(500,500));
		setLayout(new BorderLayout());
		
		panou=new ScrollPane();
		baraMeniu=new MenuBar();
		meniu=new Menu("Fisier");
		nou=new MenuItem("Nou");
		nou.addActionListener(this);
		meniu.add(nou);
		
		deschide=new MenuItem("Deschide");
		deschide.addActionListener(this);
		meniu.add(deschide);
		
		meniu.add(new MenuItem("Salvare"));
		meniu.add(new MenuItem("Salvare ca"));
		meniu.addSeparator();
		
		iesire=new MenuItem("Iesire");
		iesire.addActionListener(this);
		meniu.add(iesire);
		
		baraMeniu.add(meniu);
		baraMeniu.add(new Menu("Ajutor"));
		
		setMenuBar(baraMeniu);
		
		text=new TextArea();
		panou.add(text);
		add(panou, BorderLayout.CENTER);
		
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				System.exit(0);
			}
		});
		
		pack();
	}
	
	public void actionPerformed(ActionEvent e) {
		MenuItem apasat=(MenuItem)e.getSource();
		if (apasat.getLabel().equals("Iesire")) {
			System.exit(0);
		}
		else if(apasat.getLabel().equals("Nou"))
		{
			text.setText("DocNou");
		}
	}
	
	public static void main(String[] args) {
		Notepad p=new Notepad();
		p.setVisible(true);
	}
}
