import javax.swing.*;
import java.awt.event.*;

public class Fereastra extends JFrame implements ActionListener{
	private JButton[] butoane;
	private JTextField text;
	private JPanel panou;
	
	Fereastra(){
		butoane=new JButton[3];
		panou=new JPanel();
		for (int i=0; i<3; ++i)
		{
			butoane[i]=new JButton("" + (i+1));
			butoane[i].addActionListener(this);
			panou.add(butoane[i]);
		}
		
		//adaug actiunea de inchidere
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				System.exit(0);
			}
		});
		//this.setLayout(new BorderLayout());
		text=new JTextField("   ");
		panou.add(text);
		this.add(panou);
		pack();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		JButton butonApasat=(JButton)e.getSource();
		text.setText(butonApasat.getLabel());
	}
	
	public static void main(String[] args) {
		Fereastra f=new Fereastra();
		f.setVisible(true);
	}
}
