from hashlib import md5

def main():
    nume_fisier=raw_input("Introduceti nume fisier")
    try:
        fisier=open(nume_fisier, "r")
        continutFisier=fisier.read()
        fisier.close()
        continutMD5=md5(continutFisier).hexdigest()

    except IOError:
        printf("Eroare la deschiderea fisierului de intrare %s" %nume_fisier)
        return

    try:
        fisierIesire=open("%s.md5" %nume_fisier, "w")
        fisierIesire.write(continutMD5)
        fisierIesire.close()

    except IOError:
        print("Eroare la  fisierul de iesire")
        return
    print ("SUCCES")
main()
