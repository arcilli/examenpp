import java.util.ArrayDeque;
import java.util.Queue;

public class ManagerThread extends Thread{
	private int nrThreads;
	private Queue<Integer> val=new ArrayDeque<Integer>();
	
	ManagerThread(int n, Queue<Integer>lista){
		nrThreads=n;
		val=lista;
	}
	
	@Override
	public void run() {
		for (int i=0; i<nrThreads; ++i) {
			new SumThread(val.poll()).start();
		}
	}
}
