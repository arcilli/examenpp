public class SumThread extends Thread{
	private int number, suma=0;
	SumThread(int n){
		number=n;
	}
	
	@Override
	public void run(){
		for (int i=1; i<=number; ++i) {
			suma+=i;
		}
		System.out.println("Suma pentru n= " + number + " este: "+suma);
	}
}
