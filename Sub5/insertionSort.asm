org 100h
.data
    vector db 6,9,0,1,5,7,8
    n dw 7 ;nr de elemente

.code
    mov cx, [n] ;n
    mov bx, 1   ;i=1
    bucla_for:
        mov dl, [vector+bx]
        mov si, bx; poz=i
        bucla_while:
            cmp si, 0   ;poz>0?
            je sfarsit_while
            mov al, [vector+si-1]   ;v[poz-1]
            cmp ax, dx  ;v[poz-1]>temp>?
            jle sfarsit_while
            mov [vector+si], al  ;v[poz[=v[poz-1]
            dec si  ;--poz
            jmp bucla_while
        sfarsit_while:
            mov [vector+si], dl  ;v[poz]=temp
        inc bx  ;++i
        cmp bx, cx  ;i<n?
        jl bucla_for