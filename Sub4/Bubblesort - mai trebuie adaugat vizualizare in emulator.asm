org 100h
.data                               
    vector db 5, 1, 7, 3, 6, 4
    n dw 6 ;numarul de elemente
    
.code
        mov cx, [n] ;n=nr. de elemente
        dec cx  ;k=n-1
        bucla_do:
            xor ax, ax  ;schimb=false
            xor si, si  ;i=0
            bucla_for:
                mov bl, [vector+si] ;v[i]
                mov dl, [vector+si+1] ;v[i+1]
                cmp bl, dl  ; v[i]>v[i+1]?
                jle no_swap
                xchg bl, dl;    swap(v[i], v[i+1]) din registri
                mov [vector+si], bl
                mov [vector+si+1], dl
                inc ax  ;schimb=true
                no_swap:
                    inc si  ;++i
                    cmp si, cx  ;i<k?
                    jl bucla_for
                dec cx   ;--k
                cmp ax, 0    ;schimb==false
                jnz bucla_do