import javax.swing.*;
import java.awt.event.*;
import java.awt.BorderLayout;
import java.awt.Dimension;

public class Notepad extends JFrame implements ActionListener{
	private static final long serialVersionUID = 1L;
	private JMenuBar baraMeniu;
	private JMenu meniu;
	private JScrollPane panou;
	private JTextArea text;
	private JMenuItem nou, deschide, iesire;
	
	public Notepad() {
		super("Notepad cu SWING");
		setPreferredSize(new Dimension(500,500));
		setLayout(new BorderLayout());
		baraMeniu=new JMenuBar();
		
		meniu=new JMenu("Fisier");
		
		nou=new JMenuItem("Nou");
		nou.addActionListener(this);
		meniu.add(nou);
		
		deschide=new JMenuItem("Deschide");
		deschide.addActionListener(this);
		meniu.add(deschide);
		
		meniu.add(new JMenuItem("Salvare"));
		meniu.add(new JMenuItem("Salvare ca"));
		
		iesire=new JMenuItem("Iesire");
		iesire.addActionListener(this);
		meniu.addSeparator();
		meniu.add(iesire);
		
		baraMeniu.add(meniu);
		baraMeniu.add(new JMenu("Ajutor"));
		
		setJMenuBar(baraMeniu);
		
		text=new JTextArea();
		panou=new JScrollPane(text);
		add(panou, BorderLayout.CENTER);
		
		//ca sa ies
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				System.exit(0);
			}
		});
		pack();
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		JMenuItem menuApasat=(JMenuItem)e.getSource();
		if (menuApasat.getText().equalsIgnoreCase("Iesire"))
			System.exit(0);
		else if (menuApasat.getText().equals("Nou"))
		{
			text.setText("Document nou");
		}
	}
	
	public static void main(String[] args) {
		Notepad p=new Notepad();
		p.setVisible(true);
	}
}
