import java.awt.Graphics;

public class Patrat implements Nava{
	int x, y, l;
	
	public Patrat(int x, int y, int l) {
		this.x=x;
		this.y=y;
		this.l=l;
	}
	
	@Override
	public void paint(Graphics g) {
		g.drawRect(x, y, l, l);
	}
}
