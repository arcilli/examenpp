import java.awt.*;
import java.awt.event.*;

public class FereastraNave extends Frame{
	private Canvas panza;
	private String forma=new String("cerc");
	private Fabrica fabrica = new FabricaNave();

	public FereastraNave() {
		super("Fabrica de nave");
		setPreferredSize(new Dimension(500,500));

		panza=new Canvas() {
			public void paint(Graphics g) {
				g.clearRect(0, 0, getWidth(), getHeight());
				try {
					fabrica.make(forma).paint(g);
				} catch (Exception e1) {
					e1.printStackTrace();
				}
			}
		};
		
		add(panza);
		
		addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent e) {
				if(e.getKeyChar()=='p')
				{
					forma="patrat";
				}
				else if(e.getKeyChar()=='t')
				{
					forma="triunghi";
				}
				else if(e.getKeyChar()=='c')
				{
					forma="cerc";
				}
				panza.repaint();
			}
		});
		
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				System.exit(0);
			}
		});
		
		pack();
	}
	
	public static void main(String[] args) {
		FereastraNave f=new FereastraNave();
		f.setVisible(true);
	}
}
