import java.awt.Graphics;
public class Cerc implements Nava{
	private int x, y, r;
	
	public Cerc(int x, int y, int r) {
		this.x=x;
		this.y=y;
		this.r=r;
	}
	
	@Override
	public void paint(Graphics g) {
		g.drawOval(x, y, r, r);
	}
}
