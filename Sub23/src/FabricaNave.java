
public class FabricaNave implements Fabrica{

	@Override
	public Nava make(String tipNava) throws Exception {
		if (tipNava.equalsIgnoreCase("patrat"))
		{
			return new Patrat(200,200,50);
		}
		if (tipNava.equalsIgnoreCase("cerc"))
		{
			return new Cerc(200,200,50);
		}
		if (tipNava.equals("triunghi"))
		{
			return new Triunghi(150,200, 350,222, 450, 450);
		}
		else
		{
			throw new Exception("Nu s-a putut crea forma"+tipNava);
		}
	}
}
