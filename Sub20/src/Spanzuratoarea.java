import java.awt.*;
import java.awt.event.*;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JPanel;

public class Spanzuratoarea extends Frame{
	private boolean ghicit=false;
	private int nrIncercari=0;
	private Map <Integer, String> cuvinte;
	private String cuvantCautat;
	private String cuvantGhicit;
	private String cuvantCuUnderscore;
	private String mesaj;
	
	private Canvas panzaDesen=new Canvas() {
		public void paint(Graphics g) {
			g.clearRect(0, 0,  getWidth(),  getHeight());
			g.drawLine(20,380,100,380);
			g.drawLine(60,380,60,30);
			g.drawLine(60,30,200,30);
			g.drawLine(200,30,200,60);
			
			g.drawString(cuvantCuUnderscore,  200, 300);
			if (ghicit)
			{
				g.drawString(mesaj, 250,150);
			}
			if (nrIncercari>=1) g.drawOval(180, 60, 40, 40);	//cap
			if (nrIncercari>=2) g.drawLine(200, 100, 200, 160);	//corp
			if (nrIncercari>=3)	g.drawLine(200, 120, 170, 100);	//stanga
			if (nrIncercari>=4)	g.drawLine(200, 120, 230, 100);	//dreapta
			if (nrIncercari>=5) g.drawLine(200, 160, 170, 180);	//stangul
			if (nrIncercari>=6) g.drawLine(200, 160, 230, 180);	//dreptul
		}
	};
	
	public void afisareCuvantCuUnderscore(String cuvant) {
		cuvantCuUnderscore=new String("");
		for (int i=0; i<cuvant.length(); ++i) {
			char c=cuvant.charAt(i);
			if (c==' ') {
				cuvantCuUnderscore+="_  ";
			}
			else
				cuvantCuUnderscore+=(c+" ");
		}
		panzaDesen.repaint();
	}
	
	public String afisareLiteraInCuvant(char litera) {
		String cuvantGhicitNou=new String("");
		for (int i=0; i<cuvantCautat.length(); ++i) {
			char caracterCautat=cuvantCautat.charAt(i);
			char caracterGhicit=cuvantGhicit.charAt(i);
			
			if (caracterCautat == litera) {
				//litera EXISTA in cuvantul cautat
				cuvantGhicitNou+=litera;
			}
			else if(caracterGhicit <='Z' && caracterGhicit >='A')
				cuvantGhicitNou+=caracterGhicit;
			else
				cuvantGhicitNou+=" ";
		}
		return cuvantGhicitNou;
	}
	
	public Spanzuratoarea() {
		super("Spanzuratoarea");
		setPreferredSize(new Dimension(600,400));
		cuvinte=new HashMap<Integer, String>();
		cuvinte.put(0, "ORTOPANTOGRAFIE");
		cuvinte.put(1, "ABECEDAR");
		cuvinte.put(2, "MAREA MARMARA");
		
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				System.exit(0);
			}
		});
		
		addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent e) {
				if (ghicit==false) {
					int tasta=e.getKeyCode();
					int indexLitera=cuvantCautat.indexOf((char)tasta);
					if (indexLitera != -1)	//gasesc
					{
						cuvantGhicit=afisareLiteraInCuvant((char)tasta);
						afisareCuvantCuUnderscore(cuvantGhicit);
						if (cuvantGhicit.indexOf(' ')==-1)
						{
							ghicit=true;
							mesaj=new String("Brava");
							panzaDesen.repaint();
						}
					}
					else
					{
						//gresit, desenez o parte a corpului
						nrIncercari++;
						if (nrIncercari==6) {
							ghicit=true;
							mesaj=new String("perdut");
						}
						panzaDesen.repaint();
					}
				}
			}
		});
		
		//int indiceCuvant=(int)Math.round(Math.random()*10);
		int indiceCuvant=0;
		cuvantCautat=cuvinte.get(indiceCuvant);
		System.out.println(cuvantCautat);
		cuvantGhicit=new String();
		
		for (int i=0; i<cuvantCautat.length(); ++i)
		{
			cuvantGhicit+=" ";
		}
		
		afisareCuvantCuUnderscore(cuvantGhicit);
		mesaj=new String("Apasati taste & incearca");
		add(panzaDesen);
		setResizable(false);
		pack();
	}
	
	public static void main(String[] args) {
		Spanzuratoarea s=new Spanzuratoarea();
		s.setVisible(true);
	}
}
