from logic import *

def main():
    a=input("A")
    b=input("B")
    c=input("C")

    #initializare porti
    a1=And("And1")
    a2=And("And2")
    a3=And("And3")

    x1=Xor("Xor1")
    n1=Not("Not 1")
    n0=Not("Not 0")
    or1=Or("Or 1")

    #conectare
    a1.C.connect(a2.A)
    
    n1.B.connect(a1.B)
    n0.B.connect(n1.A)
    n0.B.connect(a3.A)
    or1.C.connect(n0.A)
    
    a2.C.connect(x1.A)
    a3.C.connect(x1.B)
    
    #adaugarea monitor
    x1.C.monitor=1

    #adaugare intrari
    a1.A.set(a)
    a1.B.set(b)

    or1.A.set(c)
    or1.B.set(a)
    a3.B.set(b)
main()
