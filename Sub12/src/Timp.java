
public class Timp {
	private int ore, minute;
	
	public Timp() {
		ore=minute=0;
	}
	
	public Timp(int ore, int minute) {
		this.ore=ore;
		this.minute=minute;
	}
	
	public int getOre() {
		return ore;
	}
	
	public int getMinute() {
		return minute;
	}
	
	public void incerementareMinute() {
		if (59 == minute) {
			minute=0;
			ore++;
		}
		else {
			minute++;
		}
		if (24 == ore) {
			ore=0;
		}
		System.out.println("Minutele au fost incrementate");
	}
	
	public void memoreazaNouaStare() {
		System.out.println("A fost memorata noua stare");
	}
}
