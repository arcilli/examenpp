
public class Buton {
	private String name;
	private int nrApasari=0;
	
	public Buton() {
		name=new String("");
	}
	
	public Buton(String nume) {
		this.name=new String(nume);
	}
	
	public void apasa() {
		++nrApasari;
	}
	
	public int getNrApasari()
	{
		return nrApasari;
	}
}
