
public class CeasSimplu {
	private Timp t;
	private Ecran e;
	private Buton b1, b2;
	
	public CeasSimplu() {
		t=new Timp();
		e=new Ecran();
		b1=new Buton("Buton1");
		b2=new Buton("Buton2");
	}
	
	public CeasSimplu(int ore, int minute) {
		t=new Timp(ore, minute);
		e=new Ecran();
		b1=new Buton("Buton1");
		b2=new Buton("Buton2");
	}
	
	public void apasButon1()
	{
		b1.apasa();
		if(b1.getNrApasari()%2==1)
			e.clipescOrele();
		else
			e.clipescMinutele();
	}
	
	public void apasButon2() {
		b2.apasa();
		t.incerementareMinute();
		e.refresh();
	}
	
	public void apasButon1si2() {
		t.memoreazaNouaStare();
		e.opresteClipitul();
	}
	
	public void afisareTimp() {
		int nrOre=t.getOre();
		int nrMinute=t.getMinute();
		System.out.print("Este ora: " + nrOre);
		System.out.println(" : " + nrMinute);
	}
}
