.model small
org 100h
.data
    vector db 1, 4, 5, 10, 6
    n dw 5 ; numarul de elemente
.code
    lea bx, vector
    xor dx, dx ; aici se va gasi suma
    xor si, si
    bucla:
        add dl, [vector+si]
        inc si
        cmp si, [n]
        jl bucla