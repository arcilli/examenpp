from logic import *

def main():
    a=input("A= ")
    b=input("B= ")
    c=input("C= ")
    
    x1=Xor('Xor1')
    x2=Xor('Xor2')

    o=Or('Or')

    a1=And('And1')
    a2=And('And2')

    #conectare
    x1.C.connect(x2.A)
    x1.C.connect(a2.A)
    a2.C.connect(o.A)
    a1.C.connect(o.B)

    #monitorizare iesiri
    x2.C.monitor=1
    o.C.monitor=1

    #setare iesiri
    x1.A.set(a)
    x1.B.set(b)
    a1.A.set(a)
    a1.B.set(b)
    a2.B.set(c)
    x2.B.set(c)

main()
