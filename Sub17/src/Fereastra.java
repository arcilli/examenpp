import javax.swing.*;

import java.awt.Dimension;
import java.awt.event.*;
public class Fereastra extends JFrame{
	private JLabel text1;
	private JLabel text2;
	private JLabel coordX;
	private JLabel coordY;
	private JPanel panou;
	
	Fereastra(){
		super("Coordonate in SWING");
		setPreferredSize(new Dimension(500, 500));
		text1=new JLabel("Coord X: ");
		text2=new JLabel("Coord Y:"); 
		coordX=new JLabel();
		coordY=new JLabel();
		panou=new JPanel();
		
		panou.add(text1);
		panou.add(coordX);
		panou.add(text2);
		panou.add(coordY);
		
		this.add(panou);
		pack();
		
		this.setResizable(false);
	
		this.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				coordX.setText(new Integer(e.getX()).toString());
				coordY.setText(new Integer(e.getY()).toString());
			}
		});
		
		this.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				System.exit(0);
			}
		});
	}
	
	public static void main(String[] args) {
		Fereastra f=new Fereastra();
		f.setVisible(true);
	}
}
