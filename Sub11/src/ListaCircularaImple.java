
public class ListaCircularaImple<E> implements ListaCirculara<E>{
	private class Node
	{
		E value;
		Node next;

		protected Node(E value)
		{
			this.value=value;
			this.next=null;
		}
	};

	private Node head=null;
	private Node tail=null;

	public ListaCircularaImple(){
		head=tail=null;
	}
	@Override
	public boolean addElement(E val) {
		if ((head == null) || tail ==null)
		{
			//pun primul element
			Node nod=new Node(val);
			head=tail=nod;
			tail.next=head;
			return true;
		}
		else{
			Node nod=new Node(val);
			nod.next=head;
			head=nod;
			tail.next=head;
			return true;
		}
	}

	@Override
	public void print() {
		Node p=head;
		System.out.println(p.value);
		p=head.next;
		while (p.next != head)
		{
			System.out.println(p.value);
			p=p.next;
		}
		System.out.println(p.value);
	}
	@Override
	public boolean deleteElement(E val) {
		Node p=head;
		if (p.value == val)
		{
			tail.next=head.next;
			Node q = head;
			head=head.next;
			q = null;
			return true;
		}
		else
		{
			p=p.next;
			//trebuie sa caut
			while (p.next.value != val && p!=head)
			{
				p=p.next;
			}
			if (p.next.value == val)
			{
				Node q =p.next;
				p.next=q.next;
				q=null;
				return true;
			}
		}
		return false;
	}
}