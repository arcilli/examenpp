
public interface ListaCirculara<E> {
	public boolean addElement(E val);
	public void print();
	public boolean deleteElement(E val);
}
