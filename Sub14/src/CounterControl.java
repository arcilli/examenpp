import java.util.Scanner;

public class CounterControl {
	public static void main(String[] args) {
		int nr;
		Scanner scan=new Scanner(System.in);
		nr=scan.nextInt();
		scan.close();
		CounterEvent b=new CounterEvent();
		Counter c=new Counter(b, nr);
		Receptor r=new Receptor(b, nr);
		c.start();
		r.start();
	}
}
