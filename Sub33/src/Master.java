
public class Master {
	private int suma=0;
	Master(int n){
		Slave slaves[]=new Slave[4];

		slaves[0]=new Slave(1, n/4);
		slaves[1]=new Slave(n/4, n/2);
		slaves[2]=new Slave(n/2, 3*n/4);
		slaves[3]=new Slave(3*n/4, n+1);

		for (int i=0; i<4; ++i){
			slaves[i].start();
			try {
				slaves[i].join();
			}catch(InterruptedException e) {
				e.printStackTrace();
			}
			System.out.println("Slave-ul " +(i+1) + "a terminat de calculat suma" +slaves[i].getSuma());
			suma+=slaves[i].getSuma();
		}
	}
	
	public int getSuma() {
		return suma;
	}
}
