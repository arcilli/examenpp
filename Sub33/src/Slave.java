
public class Slave extends Thread{
	private int n1, n2;
	private int suma=0;
	
	Slave(int n1, int n2){
		this.n1=n1;
		this.n2=n2;
	}
	
	@Override
	public void run() {
		for (int i=n1; i<n2; ++i)
		{
			suma+=i;
		}
	}
	
	public int getSuma() {
		return suma;
	}
}
