import java.awt.*;
import java.awt.event.*;

public class Fereastra extends Frame{
	private Label text1, text2, coord1, coord2;
	
	public Fereastra() {
		super("Fereastra cu click");
		this.setLayout(new FlowLayout());
		this.setPreferredSize(new Dimension(500,500));
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				System.exit(0);
			}
		});
		text1=new Label("Coord X: ");
		text2=new Label("Coord Y: ");
		coord1=new Label("    ");
		coord2=new Label("    ");
		Panel panou=new Panel();
		panou.add(text1);
		panou.add(coord1);
		panou.add(text2);
		panou.add(coord2);
		this.add(panou);
		
		pack();
		this.setResizable(false);
		
		this.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				coord1.setText(""+e.getX()+"");
				coord2.setText(""+e.getY()+"");
			}
		});
	}
	
	public static void main(String[] args) {
		Fereastra f=new Fereastra();
		f.setVisible(true);
	}
}