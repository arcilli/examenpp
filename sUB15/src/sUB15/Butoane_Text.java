package sUB15;

import java.awt.*;
import java.awt.event.*;

public class Butoane_Text extends Frame implements ActionListener{
	private Button butoane[];
	private TextField text;
	private Panel panou;
	
	Butoane_Text(){
		super("Fereastra cu 3 butoane si label");
		butoane=new Button[3];
		panou=new Panel();
		for (int i=0; i<3; ++i) {
			butoane[i]=new Button(""+ (i+1));
			butoane[i].addActionListener(this);
			panou.add(butoane[i]);
		}
		text=new TextField();
		this.setLayout(new BorderLayout());
		this.add(panou);
		panou.add(text);
		
		pack();
		//adauga cum sa se termine povestea
		addWindowListener(new WindowAdapter (){
			public void windowClosing(WindowEvent e) {
				System.exit(0);
			}
		});
	}
	
	public void actionPerformed(ActionEvent e) {
		Button butonApasat=(Button)(e.getSource());
		text.setText(butonApasat.getLabel());
	}
	
	public static void main(String[] args) {
		Butoane_Text b=new Butoane_Text();
		b.setVisible(true);
	}
}